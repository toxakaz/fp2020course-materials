type 'self lam =
  | Var of string
  | Abs of string * 'self lam
  | App of 'self lam * 'self lam

type typname = string

type typ = Ground of typname | Arrow of typ * typ

let gensym () = assert false

module Env : sig
  type t
end = struct
  type t
end

module Subst : sig
  type t

  val replace : t -> typname -> typ -> t

  val merge : t -> t -> t
end = struct
  type t = (string * typ) list

  let replace : t -> typname -> typ -> t =
   fun _ _ _ -> assert false

  let merge _ _ = assert false
end

(* свертка с дополнительной херней *)
let rec para abs app e xs =
  match xs with
  | Var s ->
      let typ = gensym () in
      (Ground typ, [ (s, typ) ])
  | Abs (_, _) -> assert false
  | App (l, r) -> (
      match (para abs app e l, para abs app e r) with
      | (Ground t1, e1), (Ground t2, e2) ->
          (* let r = gensyn () in
             let e1 =
               Subst.replace e1 t1
                 (Arrow (Ground t2, Ground r))
             in *)
          assert false
      (* | Arrow (l, r), k when l = k -> r *)
      | _ -> failwith "not typable" )

(* | x :: xs -> f x (xs, paraL f e xs) *)
