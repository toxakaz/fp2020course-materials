let rec fac n = if n <= 1 then 1 else n * fac (n - 1)

let%test _ = 120 = fac 5

let rec fix f x = f (fix f) x

let rec ygrek f = f (ygrek f)

(*
f (f p) === f p === p === f p   where p = ygrek f
*)

let fac self n = if n <= 1 then 1 else n * self (n - 1)

let%test _ = 120 = fix fac 5

(* Defining catamorphism and anamorphisms via Fixpoint for types *)
module Fix0 (X : sig
  type 'self t
end) =
struct
  type t = F of t X.t
end

module Fix1 (X : sig
  type ('self, 'a) t
end) =
struct
  type 'a t = F of ('a t, 'a) X.t
end

(* ***************** Lists ****************************** *)
type 'a list = Nil | Cons of 'a * 'a list

type nonrec ('self, 'a) listlike =
  | Nil
  | Cons of 'a * 'self

module L = struct
  module M = Fix1 (struct
    type ('a, 'b) t = ('a, 'b) listlike
  end)

  include M

  let cons x xs = F (Cons (x, xs))

  let nil = F Nil

  let rec of_list = function
    | [] -> F Nil
    | x :: xs -> F (Cons (x, of_list xs))

  let rec to_list = function
    | F Nil -> []
    | F (Cons (x, xs)) -> x :: to_list xs

  let rec rev_append sx ys =
    match sx with
    | F Nil -> ys
    | F (Cons (x, sx)) -> rev_append sx (cons x ys)

  let merge xs ys =
    let rec helper acc = function
      | F Nil, ys | ys, F Nil -> rev_append acc ys
      | F (Cons (x, xs)), (F (Cons (y, _)) as r) when x < y
        ->
          helper (cons x acc) (xs, r)
      | l, F (Cons (y, ys)) -> helper (cons y acc) (l, ys)
    in
    helper nil (xs, ys)

  let%test _ =
    merge (of_list [ 1; 2; 3 ]) (of_list [ 4; 5; 6 ])
    = of_list [ 1; 2; 3; 4; 5; 6 ]

  let%test _ =
    merge (of_list [ 1; 3; 5 ]) (of_list [ 2; 4; 6 ])
    = of_list [ 1; 2; 3; 4; 5; 6 ]

  (* fold_left  *)
  let rec cata f acc xs =
    match xs with
    | F Nil -> acc
    | F (Cons (x, tl)) -> cata f (f acc x) tl

  let sum xs = cata ( + ) 0 xs

  let%test _ = sum (of_list [ 1; 2; 3 ]) = 6

  (* unfoldr *)
  let rec ana fin f pred init =
    if fin init then nil
    else cons (f init) (ana fin f pred (pred init))
end

let%test _ =
  let n = 10 in
  let xs = List.init n (fun x -> x + 1) in
  let sum xs = L.cata ( + ) 0 (L.of_list xs) in
  n * (n + 1) / 2 = sum xs

let rec hylo f e p g h eta =
  if p eta then e else hylo f (f e (g eta)) p g h (h eta)

let%test _ =
  let fact = hylo ( * ) 1 (( = ) 0) Fun.id pred in
  fact 5 = 120

(* ********************************************************* *)

type nat = Zero | Succ of nat

(*
  nat ~= unit list
*)

type 'self natlike = Zero | Succ of 'self

module N = struct
  include Fix0 (struct
    type 'a t = 'a natlike
  end)

  let zero = F Zero

  let succ n = F (Succ n)

  (* For peano number left fold is the same as right fold *)
  let rec cata f acc = function
    | F Zero -> acc
    | F (Succ p) -> cata f (f acc) p

  (* unfold *)
  let rec ana fin pred init =
    if fin init then zero
    else succ (ana fin pred (pred init))

  let of_int n = ana (( = ) 0) (fun x -> x - 1) n

  let%test _ = succ (succ zero) = of_int 2

  let to_int : t -> int = cata (fun n -> n + 1) 0

  let%test _ = to_int zero = 0

  let%test _ = to_int (succ zero) = 1

  let%test _ = to_int (succ (succ zero)) = 2

  (* Paramorphism: Храним дополнительно текущее посчитанное значение *)
  let rec para f n =
    match n with
    | F Zero -> f Zero
    | F (Succ p) -> f (Succ (p, para f p))

  let natfac =
    let alg = function
      | Zero -> 1
      | Succ (n, f) -> to_int (succ n) * f
    in
    para alg

  let%test _ = natfac (succ (succ (succ zero))) = 6
end

type 'a tree =
  | Leaf
  | Empty of 'a
  | Node of 'a tree * 'a tree

type nonrec ('self, 'a) treelike =
  | Leaf
  | Empty of 'a
  | Node of 'self * 'self

module T = struct
  include Fix1 (struct
    type ('a, 'b) t = ('a, 'b) treelike
  end)

  (* Smart constructors :) *)
  let leaf = F Leaf

  let empty n = F (Empty n)

  let node a b = F (Node (a, b))

  (* left and then right traversal *)
  let rec cata f acc xs =
    match xs with
    | F Leaf -> acc
    | F (Empty n) -> f acc n
    | F (Node (l, r)) -> cata f (cata f acc l) r
end

let rec myhylo :
    (('a -> 'b) -> 'c -> 'd) ->
    ('d -> 'b) ->
    ('a -> 'c) ->
    'a ->
    'b =
 fun fmap alg coalg t ->
  alg (fmap (myhylo fmap alg coalg) (coalg t))

(* Can't express merge sort yet. need rectypes *)

(*
let mergeSort =
  let alg : (int L.t, int) treelike -> int L.t = function
    | Leaf -> L.nil
    | Empty c -> L.(cons (c : int) nil)
    | Node (l, r) -> L.merge l r
  in
  let coalg = function
    | Nil -> T.leaf
    | Cons (x, Nil) -> T.empty x
    | xs ->
        let l, r = L.split_at (L.length xs / 2) xs in
        T.node l r
  in
  let fmap :
      (int L.t -> int L.t) ->
      ((('a, int) listlike as 'a), int) treelike ->
      ((('c, int) listlike as 'c), int) treelike =
   fun f -> function
    | Leaf -> Leaf
    | Empty n -> Empty n
    | Node (l, r) -> Node (f l, f r)
  in

  myhylo fmap alg coalg

let%test _ =
  L.of_list [ 1; 2; 3 ] = mergeSort (L.of_list [ 3; 2; 1 ])

let%test _ =
  L.of_list [ 1; 2; 3; 4 ]
  = mergeSort (L.of_list [ 3; 2; 1; 4 ])
*)
