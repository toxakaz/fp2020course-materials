(* Библиотека парсер-комбинаторов от печки *)
type input = char list

type 'a parse_result = Failed | Parsed of 'a * input

type 'a parser = input -> 'a parse_result

(* ***************** Простой парсер 1 ********************* *)
let return x : _ parser = fun s -> Parsed (x, s)

let char c : _ parser = function
  | h :: tl when c = h -> Parsed (c, tl)
  | _ -> Failed

let%test _ = char 'a' [ 'a'; 'b' ] = return 'a' [ 'b' ]

let%test _ = char 'b' [ 'a'; 'b' ] = Failed

let satisfy cond : _ parser =
 fun s ->
  match s with
  | h :: tl when cond h -> return h tl
  | _ -> Failed

let char c = satisfy (Char.equal c)

let digit_c =
  let is_digit ch =
    let c = Char.code ch in
    Char.code '0' <= c && c <= Char.code '9'
  in
  satisfy is_digit

let%test _ = digit_c [ '0' ] = return '0' []

let%test _ = digit_c [ 'a' ] = Failed

(* ****************** Простой парсер 2 ********************************** *)
(* произносится bind, или 'a затем' *)
let ( >>= ) : _ parser -> (_ -> _ parser) -> _ parser =
 fun p f s ->
  match p s with
  | Failed -> Failed
  | Parsed (h, tl) -> f h tl

(* ******************** Простые комбинаторы ***************************** *)
let ( *> ) : _ parser -> _ parser -> _ parser =
 fun p1 p2 -> p1 >>= fun _ -> p2

let%test _ =
  let p1 = char 'a' *> char 'b' in
  return 'b' [] = p1 [ 'a'; 'b' ]

let ( <* ) : _ parser -> _ parser -> _ parser =
 fun p1 p2 ->
  p1 >>= fun h ->
  p2 >>= fun _ -> return h

let%test _ =
  let p2 = char 'a' <* char 'b' in
  return 'a' [] = p2 [ 'a'; 'b' ]

let digit =
  digit_c >>= fun c -> return (Char.code c - Char.code '0')

let%test _ = digit [ '5' ] = return 5 []

(* *************************************************************** *)
let many (p : _ parser) : _ list parser =
  let rec helper s =
    match p s with
    | Failed -> return [] s
    | Parsed (x, tl) ->
        (helper >>= fun xs -> return (x :: xs)) tl
  in
  helper

let%test _ =
  let p = many (char 'a') in
  let input = [ 'b'; 'a'; 'b' ] in
  Parsed ([], input) = p input

let%test _ =
  let p = many (char 'b') in
  let input1 = [ 'b'; 'b' ] in
  let input2 = [ 'a' ] in
  Parsed (input1, input2) = p (List.append input1 input2)

let many1 p : _ list parser =
  p >>= fun x ->
  many p >>= fun xs -> return (x :: xs)

let%test _ =
  let p = many1 (char 'b') in
  let input1 = [ 'b'; 'b' ] in
  let input2 = [ 'a' ] in
  Parsed (input1, input2) = p (List.append input1 input2)

let%test _ =
  let p = many1 (char 'a') in
  let input = [ 'b'; 'a'; 'b' ] in
  Failed = p input

(* ************** Alternatives  ********************************** *)
let ( <|> ) : _ parser -> _ parser -> _ parser =
 fun p1 p2 s -> match p1 s with Failed -> p2 s | ok -> ok

let a_or_b = char 'a' <|> char 'b'

let%test _ = a_or_b [ 'a' ] = return 'a' []

let%test _ = a_or_b [ 'b' ] = return 'b' []

let%test _ = a_or_b [ 'c' ] = Failed

module Arithmetic1 = struct
  type expr = Const of int | Plus of expr * expr
  [@@deriving show]

  (* <expr> -- это <digit> + <expr> или <digit>
  *)

  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec p s =
      (const
      >>= (fun l ->
            char '+' >>= fun _ ->
            p >>= fun r -> return (Plus (l, r)))
      <|> const)
        s
    in
    p

  let%test _ =
    let input = [ '5'; '+'; '6'; '+'; '9' ] in
    let rez = parser input in
    (* Format.printf "%a\n%!" (pp_parse_result pp_expr) rez; *)
    rez
    = return (Plus (Const 5, Plus (Const 6, Const 9))) []
end

(* ********** Parsing of arithmetic BAD ******************************** *)
module Arithmetic2 = struct
  type expr = Const of int | Plus of expr * expr

  (* <expr> -- это <expr> + <digit>  или <digit>
     Левая рекурсия стреляет в ногу
  *)
  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec p s =
      ( p >>= fun l ->
        char '+' >>= fun _ ->
        const >>= fun r -> return (Plus (l, r)) <|> const )
        s
    in
    p

  let%test _ =
    try
      let (_ : bool) =
        parser [ '1'; '+'; '2' ]
        = return (Plus (Const 1, Const 2)) []
      in
      false
    with Stack_overflow -> true
end

(* ********** Parsing of arithmetic GOOD ******************************* *)
(* let ints_list : _ parser =
  digit >>= fun h ->
  many (satisfy (Char.equal '+') *> digit) >>= fun tl -> return (h :: tl)

let%test _ = ints_list [ '1'; '+'; '2'; '+'; '3' ] = return [ 1; 2; 3 ] []

let%test _ =
  ints_list [ '1'; '+'; '2'; '+'; '+'; '3' ] = return [ 1; 2 ] [ '+'; '+'; '3' ]

let ints_sum =
  let next = satisfy (Char.equal '+') *> digit in
  let rec helper acc s =
    match next s with
    | Failed -> return acc s
    | Parsed (n, tl) -> helper (acc + n) tl
  in
  digit >>= helper

let%test _ = ints_sum [ '1'; '+'; '2'; '+'; '9' ] = return 12 []

let%test _ =
  ints_sum [ '1'; '+'; '2'; '+'; '+'; '9' ] = return 3 [ '+'; '+'; '9' ] *)

(* ************ Parsing with parenthesis *********************************** *)

module Arithmetic3 = struct
  type expr = Const of int | Plus of expr * expr
  [@@deriving show]

  (* <expr> -- это
      либо (<expr>)
      либо <digit> и n>=0 раз + <expr>
      либо <digit>
  *)

  let choice xs =
    List.fold_left ( <|> ) (fun _ -> Failed) xs

  let parens p =
    char '(' >>= fun _ ->
    p >>= fun x ->
    char ')' >>= fun _ -> return x

  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec p s =
      choice
        [
          parens p;
          ( const >>= fun h ->
            many (char '+' >>= fun _ -> const) >>= fun tl ->
            return
              (List.fold_left
                 (fun acc r -> Plus (acc, r))
                 h tl) );
          const;
        ]
        s
    in
    p

  let%test _ =
    let input = [ '5'; '+'; '6'; '+'; '9' ] in
    let rez = parser input in
    (* Format.printf "%a\n%!" (pp_parse_result pp_expr) rez; *)
    rez
    = return (Plus (Plus (Const 5, Const 6), Const 9)) []
end

module Arithmetic4 = struct
  type expr =
    | Const of int
    | Plus of expr * expr
    | Asterisk of expr * expr
  [@@deriving show]

  (* <expr> -- это неформально (!!)
      либо (<expr>)
      либо <expr> + <expr>
      либо <expr> * <expr>
      либо <digit>
  *)

  let choice xs =
    List.fold_left ( <|> ) (fun _ -> Failed) xs

  let parens p =
    char '(' >>= fun _ ->
    p >>= fun x ->
    char ')' >>= fun _ -> return x

  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec product s =
      choice
        [
          parens sum;
          ( const >>= fun h ->
            many (char '*' >>= fun _ -> product)
            >>= fun tl ->
            return
              (List.fold_left
                 (fun acc r -> Asterisk (acc, r))
                 h tl) );
        ]
        s
    and sum s =
      choice
        [
          parens sum;
          ( product >>= fun h ->
            many (char '+' >>= fun _ -> product)
            >>= fun tl ->
            return
              (List.fold_left
                 (fun acc r -> Plus (acc, r))
                 h tl) );
        ]
        s
    in
    sum

  let%test _ =
    let input = [ '5'; '+'; '6'; '*'; '9' ] in
    let rez = parser input in
    rez
    = return
        (Plus (Const 5, Asterisk (Const 6, Const 9)))
        []

  let%test _ =
    let input = [ '5'; '*'; '6'; '+'; '9' ] in
    let rez = parser input in
    rez
    = return
        (Plus (Asterisk (Const 5, Const 6), Const 9))
        []
end

(* ********************************** The End ********************************* *)

(* Бывают разные варианты как объявить input
- char list
- int * string
- int * char Seq.t
- с поддержкой юникода
- графы

Результат парсинга тоже бывает разный. Например, можно добавить конструктор NeedMoreInput.

Или можно возвращать пустой список, если не удалось распарсить, или несколько ответов, если
можно распарсить более чем 1м способом

  type 'a parse_result = ('a * input) list

*)
