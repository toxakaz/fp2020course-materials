type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string


let eval from_env : expr -> (int, string) Result.t =
  let (>>=) x f = match x with Error e -> Error e | Ok x -> f x in
  let return = Result.ok in

  let rec helper = function
  | Const n -> return n
  | Plus  (l,r) -> helper l >>= fun l -> helper r >>= fun r -> return (l+r)
  | Slash (l,r) ->
      helper r >>= fun r ->
        if r=0 then Error "division by zero"
        else helper l >>= fun l -> return (l/r)
  | Asterisk (l,r) -> helper l >>= fun l -> helper r >>= fun r -> return (l * r)
  | Var s -> from_env s
  in
  helper



let%test _ =
  Ok 7 = eval (fun _ -> Error "not found") (Plus (Const 1, Asterisk (Const 2, Const 3)))

let%test _ =
  Error "not found" = eval (fun _ -> Error "not found") (Var "x")
