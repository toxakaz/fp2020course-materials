type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string


let eval from_env =
  let rec helper = function
  | Const n -> n
  | Plus  (l,r) -> helper l + helper r
  | Slash (l,r) -> helper l / helper r
  | Asterisk (l,r) -> helper l * helper r
  | Var s -> from_env s
  in
  helper



let%test _ =
  7 = eval (fun _ -> assert false) (Plus (Const 1, Asterisk (Const 2, Const 3)))

let%test _ =
  try
    let _ = eval (fun _ -> raise Not_found) (Var "x") in
    false
  with Not_found -> true
