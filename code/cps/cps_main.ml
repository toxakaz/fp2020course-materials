type tree = Leaf | Node of tree * tree

let n1 = 10
let n2 = 261_791    (* shouldbe OK *)

let rec make depth =
  if depth <= 0 then Leaf
  else
    let r = make (depth - 1) in
    let l = if depth mod 100000 = 0 then r else Leaf in
    Node (l, r)


let size root =
  let rec helper tree =
    match tree with Leaf -> 0 | Node (l, r) -> helper l + helper r + 1
  in
  helper root

let main =
  let n = n1 in
  Format.printf "depth = %d, size = %d\n%!" n (size (make n));
  (* Gives stack overflow *)
  let n = n2 in
  Format.printf "depth = %d, size_tail = %s\n%!" n
    (try string_of_int @@ size (make n)
     with Stack_overflow -> "<stack overflow>");
  ()


let make_tail depth =
  let rec helper acc n =
    if depth < n then acc
    else
      let l = if n mod 100000 = 0 then acc else Leaf in
      (helper[@tailcall]) (Node (l, acc)) (n+1)
  in
  helper Leaf 1


let size_tail root =
  let rec helper tree k =
    match tree with
    | Leaf -> k 0
    | Node (l, r) ->
        (helper [@tailcall]) l (fun sl ->
            (helper [@tailcall]) r (fun sr -> k (sl + sr + 1)))
  in
  helper root (fun n -> n)


let () =
  let n = n1 in
  Format.printf "depth = %d, size = %d\n%!" n (size_tail (make_tail 10));
  let n = n2 in
  Format.printf "depth = %d, size_tail = %d\n%!" n (size_tail (make_tail n));
  ()

let __ () =
  let n = 100000 in
  Format.printf "Tree: depth = %d, size = %d\n%!" n (size (make n));
  let n = 1000000 in
  (* Gives stack overflow *)
  (* Format.printf "Tree: depth = %d, size_tail = %d\n%!" n (size (make n)); *)
  (* WTF it gives stack overflow? *)
  Format.printf "Tree: depth = %d, size_tail = %d\n%!" n (size_tail (make n));
  ()
