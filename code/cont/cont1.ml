
(* ********************** Continuation ***************************************** *)
module Cont = struct
  type ('r, 'a) t = ('a -> 'r) -> 'r

  let return : 'a -> ('r, 'a) t = fun x f -> f x

  let (>>=) : ('r, 'a) t -> ('a -> ('r, 'b) t) -> ('r, 'b) t = fun x f k ->
     x (fun a -> f a k)


  let callCC f = fun k -> (f (fun a _ -> k a)) k


  let f n = (`runCont` id) $ do
        str <- callCC $ \exit1 -> do                        (* -- define "exit1" *)
          when (n < 10) (exit1 (show n))
          let ns = map digitToInt (show (n `div` 2))
          n' <- callCC $ \exit2 -> do                       (* -- define "exit2" *)
            when ((length ns) < 3) (exit2 (length ns))
            when ((length ns) < 5) (exit2 n)
            when ((length ns) < 7) $ do let ns' = map intToDigit (reverse ns)
                                        exit1 (dropWhile (=='0') ns')  --escape 2 levels
            return $ sum ns
          return $ sprintf "(ns = %s) %s" (show ns) (show n')
        return $ "Answer: " ++ str
end
