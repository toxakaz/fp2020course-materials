let fac =
  let rec helper acc i = if i <= 1 then acc else helper (acc * i) (i - 1) in
  helper 1

let%test _ = fac 5 = 120


module WithFixPoint = struct

  let fix f = (fun x -> f (fun v -> x x v)) (fun x -> f (fun v -> x x v))

  let fac2 self n = if n<=1 then 1 else n * (self (n-1))

  let%test _ = fix fac2 5 = 120
end


let rec fib_rec n =
  if n <= 1 then 1
  else fib_rec (n-1) + fib_rec (n-2)

let fib_norec f n =
  if n <= 1 then 1
  else f (n-1) + f (n-2)


(* memoization for non-recursive functions *)
let memoize f =
  let open Base in
  let table = Hashtbl.Poly.create () in
  let g x =
    match Hashtbl.find table x with
    | Some y -> y
    | None ->
      let y = f x in
      Hashtbl.add_exn table ~key:x ~data:y;
      y
  in
  g



(* memoization for recursive functions *)
let memo_rec f_norec =
  let rec f_rec_memo eta = memoize (fun x -> f_norec f_rec_memo x) eta in
  f_rec_memo

let memo_rec2 f_norec =
  (* using mutation we can put configurable function here *)
  let f = ref (fun _ -> assert false) in
  let f_rec_memo = memoize (fun x -> f_norec !f x) in
  f := f_rec_memo;
  f_rec_memo

let memo_rec3 f =
  let h = Stdlib.Hashtbl.create 11 in
  let rec g x =
    try
      Stdlib.Hashtbl.find h x
    with
      Stdlib.Not_found ->
        let y = f g x in
        Stdlib.Hashtbl.add h x y ;
        y
  in
  g

let () =
  assert (fib_rec 2 = 2)
